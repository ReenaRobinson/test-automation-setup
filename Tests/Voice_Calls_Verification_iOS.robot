*** Settings ***
Documentation    Script to verify Voice calls in iOS device
Suite Setup       Default Suite Setup
Suite Teardown    Default Suite Teardown
Library        AppiumLibrary

*** Variables ***
${REMOTE_URL}     http://192.168.1.107:4723/wd/hub      # URL to appium server
${BROWSER}            Safari
${PLATFORM_NAME}      iOS
${UDID}               37298c48a07ddf03a56d5451aae70880a0c50ff3
${PLATFORM_VERSION}    iOS 12.1.3
${DEVICE_NAME}    R�s iPhone

*** Test Cases ***
Voice Calls Verification
    [Documentation]    This script opens dialer on phone and make call to the speciifed number
    Check Voice Calls

*** Keywords ***
Default Suite Setup
    Open Application  ${REMOTE_URL}  platformName=${PLATFORM_NAME}  platformVersion=${PLATFORM_VERSION}  deviceName=${DEVICE_NAME}  udid=${UDID}

Check Voice Calls
     Click Element    name=Phone
     Click Element    xpath=//*[@text='Contacts']
     Click Element    xpath=//*[@text='Test User' and @class='UIAStaticText']
     Click Element    xpath=//*[@class='UIATable']
     sleep    7s
     Click Element    xpath=//*[@text='End call']

Default Suite Teardown
    Close All Applications
