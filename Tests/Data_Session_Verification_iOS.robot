*** Settings ***
Documentation    Script to verify Data Session in iOS device
Suite Setup       Default Suite Setup
Suite Teardown    Default Suite Teardown
Library        AppiumLibrary

*** Variables ***
${REMOTE_URL}     http://localhost:4723/wd/hub      # URL to appium server
${BROWSER}            Safari
${PLATFORM_NAME}      iOS
${UDID}               37298c48a07ddf03a56d5451aae70880a0c50ff3
${PLATFORM_VERSION}    iOS 12.1.3
${DEVICE_NAME}    R’s iPhone

*** Test Cases ***
Data Session Verification
    [Documentation]    Script opens browser and launch www.ranorex.com
    Launch Application

*** Keywords ***
Default Suite Setup
     Open Application  ${REMOTE_URL}  platformName=${PLATFORM_NAME}  platformVersion=${PLATFORM_VERSION}  deviceName=${DEVICE_NAME}  udid=${UDID}  browser=${BROWSER}

Launch Application
     Go to url  www.ranorex.com
     wait Until Page Contains    Test Automation for All    5s

Default Suite Teardown
     Close All Applications
